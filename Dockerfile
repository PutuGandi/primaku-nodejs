FROM node:alpine
RUN mkdir -p /home/app
COPY package*.json /home/app
WORKDIR /home/app
RUN npm install
COPY . .
EXPOSE 3000
CMD ["npm","start"]
# Penggunaan API Node.js

##### 1. Cek list tag image di [Docker Hub](https://hub.docker.com/r/gandi12/nodejs/tags) yang akan digunakan, lalu pull image tersebut:
```bash
docker pull gandi12/nodejs:3
```

##### 2. Aplikasi di expose dengan port 3000 (default) , menjalankan image:
```bash
docker run -p <port-local-machine>:3000 -d gandi12/nodejs:3 
```

##### 3. akses API dengan menggunakan Postman
```http
localhost:<port-local-machine>/hello
```

## Reference

[API Node.js example](https://automationrhapsody.com/build-rest-api-express-node-js-run-docker/)